
/**
 * Beschreiben Sie hier die Klasse Fisch.
 * 
 * @author (Hans Dampf) 
 * @version (eine Versionsnummer oder ein Datum)
 */
public class Fisch implements Tier
{
    // Instanzvariablen - ersetzen Sie das folgende Beispiel mit Ihren Variablen
    private String farbe;

    /**
     * Konstruktor f�r Objekte der Klasse Fisch
     */
    public Fisch()
    {
        // Instanzvariable initialisieren
        farbe = "blau";
    }

    public String lautGeben()
    {
        return "hans Dampf";
    }
}
